const mongoose = require("mongoose");
const {ObjectID} = require("mongodb");
const Schema = mongoose.Schema;

const LiveStreamSchema = new Schema({
  startTime: {type: Date},
  mediaSource: {type: String, trim: true},
  preview: {type: String, trim: true},
  streamer: {type: String, trim: true},
  title: {type: String, trim: true},
  category: {type: String, trim: true, enum: ["art", "gaming", "irl", "music"]},
  tags: {type: [String]},
  viewers: {type: Number}
});

module.exports = mongoose.model("LiveStream", LiveStreamSchema);
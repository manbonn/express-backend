const mongoose = require('mongoose');
const {ObjectID} = require("mongodb");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {type: String, trim: true},
  email: {type: String, trim: true},
  signupDate: {type: Date},
  completedStreams: {type: Number},
  followers: {type: [ObjectID]},
  following: {type: [ObjectID]},
  bio: {type: String, trim: true},
  online: {type: Boolean}
})

module.exports = mongoose.model('User', UserSchema);
const mongoose = require('mongoose');
require("../schema/LiveStream")
require("../schema/User")
const LiveStream = mongoose.model('LiveStream');
const userController = require("../controller/User")

exports.all_lives = function (req, res) {
  const allPromise = LiveStream.find({}, function (err) {
    if (err) console.log(err)
  })
  allPromise.then(it => {
    res.json(it)
  })
}

exports.get_user_by_stream = function (req, res) {
  const streamer = req.params.user
  const livePromise = LiveStream.findOne({streamer: streamer}, function(err) {
    if(err) console.log(err)
  })
  livePromise.then(it => {
    res.json(it)
  })
}

exports.start_live_stream = function (req, res) {
  const startTime = Date.now()
  const mediaSource = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
  const streamer = req.body.streamer
  const title = req.body.title
  const category = req.body.category
  const preview = "https://i.imgur.com/2F3Al82.jpeg"
  const tags = req.body.tags
  const viewers = req.body.viewers
  const liveStream = new LiveStream({
    startTime: startTime,
    mediaSource: mediaSource,
    streamer: streamer,
    title: title,
    category: category,
    preview: preview,
    tags: tags,
    viewers: viewers
  })
  liveStream.save(function (err) {
    if (err) console.log(err)
  })
}

exports.stop_live_stream = function (req, res) {
  const streamer = req.body.username
  LiveStream.findOneAndDelete({streamer: streamer}, function (err) {
    if (err) console.log(err)
  })
}

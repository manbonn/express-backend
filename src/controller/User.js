const mongoose = require('mongoose');
require("../schema/User")
const User = mongoose.model('User');

exports.all_users = function (req, res) {
  User.find({}, function (err, users) {
    if (err) res.send(err);
    res.json(users);
  });
};

exports.set_online = async function (req, res) {
  const streamer = req.body.streamer
  await User.findOneAndUpdate({username: streamer}, {online: true}, function (err) {
    if (err) return err;
  })
}

exports.get_user = async function (mail, username) {
  const userPromise = User.find({email: mail, username: username}, function (err, user) {
    if (err) return err;
    return user
  });
  let user
  await userPromise.then(it => {
    user = it
  })
  return user
};

exports.get_user_by_username = async function (req, res) {
  const username = req.params.user
  const user = await User.find({username: username}, function (err) {
    if (err) return err;
  })
  res.json(await user)
};

exports.add_user_info = function (mail, username) {
  const registrationDate = Date.now()
  const user = new User({
    username: username,
    email: mail,
    signupDate: registrationDate,
    completedStreams: 0,
    followers: [],
    following: [],
    bio: "",
    online: false
  })
  user.save(function (err) {
    if (err) console.log(err)
  })
}

exports.search = async function (req, res, query_text) {
  const query = ".*" + query_text + ".*"

  const userListPromise =
    User.find({username: new RegExp('^' + query + '$', "i")}, 'username online', function (err, res) {
      if (err) console.log(err)
      // console.log(res)
      return res
    })

  await userListPromise.then(it => res.json(it))
}

exports.logout = function (mail, username) {
//???
}

exports.follow = function (req, res) {
//  search following user, change its following list
  //search followed user -> change its followers list
}

exports.unfollow = function (req, res) {
  //  search following user, change its following list
  //search followed user -> change its followers list
}

function followed(req, res) {

}
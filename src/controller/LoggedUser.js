const mongoose = require('mongoose');
require("../schema/LoggedUser")
require("../schema/User")
const LoggedUser = mongoose.model('LoggedUser');
const userController = require("../controller/User")
const crypto = require("crypto");

exports.debug_all = function (req, res) {
  LoggedUser.find({}, function (err, users) {
    if (err) res.send(err);
    res.json(users);
  })
}

exports.debug_filter = function (req, res) {
  LoggedUser.find({username: 'abc'}, function (err, users) {
    if (err) res.send(err);
    res.json(users);
  })
}

exports.add_user = function (req, res) {
  const username = req.body.username
  const mail = req.body.email
  const password = req.body.password

  const info = areInfoAvailable(username, mail)
  if (!info.userFound && !info.mailFound) {
    const user = new LoggedUser({
      username: username,
      email: mail,
      password: password
    })
    user.save(async function (err) {
      if (err) console.log(err)
      await userController.add_user_info(mail, username)
    })
  }

  res.json({usernameTaken: info.userFound, mailTaken: info.mailFound})
};

exports.log_user_in = async function (req, res) {
  const method = req.body.method
  const user = req.body.user
  const password = req.body.password

  let foundUser = undefined
  let notFoundUser = {found: false}

  if (method === "mail") {
    const foundPromise = LoggedUser.findOne({email: user, password: password}, function (err, found) {
      if (err) res.json(err)
      return found
    });
    await foundPromise.then(it => {
        foundUser = it
    })
  } else if (method === "username") {
    const foundPromise = LoggedUser.findOne({username: user, password: password}, function (err, found) {
      if (err) res.json(err)
      return found
    });
    await foundPromise.then(it => {
      foundUser = it
    })
  } else {
    console.log("TOKEN MISSING")
  }
  if (foundUser === undefined) {
    res.json(notFoundUser)
  } else {
    if (method === "token") {
      res.json(foundUser)
    } else {
      createUserPacket(foundUser.username, foundUser.email, foundUser.password)
        .then(it => {
          res.json(it)
        })
        .catch(console.log)
    }
  }
}

async function createUserPacket(username, mail, password) {
  const word = Date.now().toString(16) + username + password + mail
  const token = crypto.createHash('sha256').update(word).digest('base64');
  const user = await userController.get_user(mail, username)
  return {
    token: token,
    info: user
  }
}

function areInfoAvailable(username, mail) {
  let userFound = false
  let mailFound = false

  LoggedUser.exists({username: username}).then(it => userFound = it)
  LoggedUser.exists({mail: mail}).then(it => mailFound = it)

  return {userFound, mailFound}
}


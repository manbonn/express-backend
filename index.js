const mongoose = require('mongoose');
const loginController = require('./src/controller/LoggedUser')
const userController = require('./src/controller/User')
const liveController = require('./src/controller/LiveStream')


const express = require('express')
const {createServer} = require("http");
const {Server} = require("socket.io");
const app = express()
const httpServer = createServer(app);
const cors = require('cors')
const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost:3500/"
  }
});
const port = 3500
const crypto = require('crypto')
mongoose.connect('mongodb://localhost:27017/test');
const db = mongoose.connection;
app.use(cors());
app.options('*', cors());
app.use(express.json());

app.get('/api/streams', (req, res) => {
  liveController.all_lives(req, res)
})

app.get('/api/users', (req, res) => {
  userController.all_users(req, res)
})

app.get('/api/search/:query', (req, res) => {
  userController.search(req, res, req.params.query)
})

app.get('/api/user/:user', (req, res) => {
  userController.get_user_by_username(req, res)
})

app.get('/api/stream/:user', (req, res) => {
  liveController.get_user_by_stream(req, res)
})

// app.get('/tags/:tag', (req, res) => {
//   res.send("reached a query for a tag")
// })

app.post('/api/login', (req, res) => {
  loginController.log_user_in(req, res)
})

app.post('/api/registration', (req, res) => {
  loginController.add_user(req, res)
})

app.post('/api/startLive', async (req, res) => {
  await liveController.start_live_stream(req, res)
  await userController.set_online(req, res)
  res.json({started: true})
})

app.post('/api/stopLive', async (req, res) => {
  await liveController.stop_live_stream(req, res)
  res.json({started: false})
})

io.on('connection', (socket) => {
  console.log("a user connected")

  socket.on('chat message', function (msg) {
    io.emit('chat message', msg)
  })

  socket.on("disconnect", function () {
    console.log("user disconnected")
  })
})

httpServer.listen(port)

// app.listen(port, () => {
//   console.log(`Express server started`)
// })


